%{
    #include <stdlib.h>
	#include <stdio.h>
	#include "y.tab.h"
	extern int numlinea;

	    int st_newline();
    int lexer_error(int line_number, char *lexem);
    extern int line_number;
%}

%option noyywrap

%%
(start|START) 		{return T_START;}
(end|END)	 		{return T_END;}
string 				{return T_STRING;}
number 				{return T_NUM;}
\+					{ return T_MAS; }
\-					{ return T_MENOS; }
\*					{ return T_POR; }
\*\*				{ return T_POT; }
\/					{ return T_DIV; }
\( 					{ return T_PAR_A; }
\) 					{ return T_PAR_C; }
\[  				{ return T_CORCH_A; }
\]					{ return T_CORCH_C; }
\{  				{ return T_LLAVE_A; }
\} 					{ return T_LLAVE_C; }
=					{ return T_IGUAL; }
\=\=				{ return T_IGUAL_IGUAL; }
\cat				{ return T_CAT; }
\cmp				{ return T_CMP; }
\charpos			{ return T_CHARAT; }
\read				{ return T_READ; }
\echo				{ return T_ECHO; }
\<					{ return T_MIN; }
\>					{ return T_MAY; }
\>\=				{ return T_MAY_IGUAL; }
\<\=				{ return T_MIN_IGUAL; }
\!\=				{ return T_DISTINTO; }
\&\&				{ return T_AND; }
\|\|				{ return T_OR; }
\if					{ return T_IF; } 
\else				{ return T_ELSE; }
\while				{ return T_WHILE; }
,					{ return T_COMA; }
\'[^']*\'			{
						yylval.string = strdup(yytext);
						return T_CADENA;
					}
[0-9]+(\.[0-9]+)?	{
						yylval.number = atof(yytext);
						return T_NUM;
					}
[a-zA-Z][a-zA-Z0-9]*	{
							yylval.id = strdup(yytext);
    						return T_ID;
						}
[a-zA-Z]*			{
						yylval.string = yytext;
						return T_STRING;
					}
[\n \t]             ;
.                   {   return lexer_error(line_number, yytext);  }

%%

