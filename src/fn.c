#include "fn.h"

struct ast_node* n_assign(char* id, struct ast_node* expr)
{
    struct ast_node* assign = ast_new_node(N_ASSIGN);
    ast_add_child(assign, n_id(id, 0));
    ast_add_child(assign, expr);

    return assign;
}

struct ast_node* n_if(struct ast_node* cond, struct ast_node* then, struct ast_node* _else){
    struct ast_node* _if = ast_new_node(N_IF);
    ast_add_child(_if, cond);
    ast_add_child(_if, then);
    if (_else != NULL) {
        ast_add_child(_if, _else);
    }

    return _if;
}

struct ast_node* n_printer(struct ast_node* exp, struct ast_node* rec){
	if(rec == NULL){
		rec = ast_new_node(N_PRINTER);
	}
	ast_add_child_first(rec, exp);
	return rec;
};

struct ast_node* n_while(struct ast_node* cond, struct ast_node* then){
    struct ast_node* _while = ast_new_node(N_WHILE);
    ast_add_child(_while, cond);
    ast_add_child(_while, then);
    return _while;
};

struct ast_node* n_declaration(int _type, struct ast_node* _var, struct ast_node* _array){
    struct ast_node* _declaration = ast_new_node(N_DECLARATION);
    st_get_symbol(_var->data.id.id)->type = _type;
    ast_add_child(_declaration, _var);
    if(_array != NULL){ //Si es un array se crea otro hijo que contiene los corchetes y la cantidad de posiciones del arreglo
        ast_add_child(_declaration, _array);
    }
    return _declaration;
};

struct ast_node* n_declaration_array(struct ast_node* _type, struct ast_node* _var){
    struct ast_node* _declaration = ast_new_node(N_DECLARATION);
    ast_add_child(_declaration, _type);
    ast_add_child(_declaration, _var);
    return _declaration;
};

struct ast_node* n_scan(struct ast_node* _type, struct ast_node* _id){
    struct ast_node* _scan = ast_new_node(N_SCAN);
    ast_add_child(_scan, _type);
    ast_add_child(_scan, _id);
    return _scan;
};

struct ast_node* n_length(struct ast_node* _id){
    struct ast_node* _length = ast_new_node(N_LENGTH);
    ast_add_child(_length, _id);
    return _length;
}

// struct ast_node* n_array(double _num){
    // struct ast_node* _array = ast_new_node(N_ARRAY);
    // ast_add_child(_array, _num);
    // return _array;
// }