// MIO
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "main.h"
#include "../lib/ast.h"
#include "fn.h"

int yyparse (void);
void print_node(struct ast_node* node);

/**
 * Manejador de errores léxicos
 *
 * @param  line_number
 * @param  lexem
 * @return
 */
int lexer_error(int line_number, char *lexem)
{
    printf("\nError léxico en la línea %d: '%s'\n", line_number, lexem);
    return -1;
}

/**
 * Manejador de errores sinácticos
 *
 * @param  line_number
 * @param  s
 * @return
 */
int syntax_error(int line_number, char *s)
{
    printf("\nError sintáctico en la línea %d (%s).\n", line_number, s);
    return -2;
}

void st_print(int id, struct usr_st_data* data)
{
    printf("    double %s = 0;\n", data->name);
}

void print_assign(struct ast_node* assign)
{
    print_node(ast_get_nth_child(assign, 0));
    printf(" = ");
    print_node(ast_get_nth_child(assign, 1));
}

void print_const_number(struct ast_node* const_number)
{
    printf("%.02f", const_number->data.number);
}

void print_operation(struct ast_node* operation)
{
    struct ast_node* operand = ast_get_nth_child(operation, 0);

    printf("(");
    if (operand->data.operand == '^') { //Potencia
        printf("pow(");
        print_node(ast_get_nth_child(operation, 1));
        printf(", ");
        print_node(ast_get_nth_child(operation, 2));
        printf(")");
    } else {
        print_node(ast_get_nth_child(operation, 1));
        print_node(operand);
        print_node(ast_get_nth_child(operation, 2));
    }
    printf(")");
}

void print_operand(struct ast_node* operand)
{
    switch(operand->data.operand){
        case '1':
            printf(" == ");
            break;
        case '2':
            printf(" != ");
            break;
        case '3':
            printf(" <= ");
            break;
        case '4':
            printf(" < ");
            break;
        case '5':
            printf(" >= ");
            break;
        case '6':
            printf(" > ");
            break;
        case '7':
            printf(" && ");
            break;
        case '8':
            printf(" || ");
            break;
        default:
           printf(" %c ", operand->data.operand);
           break;
    }
}

void print_expr(struct ast_node* expr)
{
    print_node(ast_get_nth_child(expr, 0));
}

void print_block(struct ast_node* block)
{
    struct ast_node* node = ast_get_nth_child(block, 0);
    printf("    printf(\"%%.02f\\n\", ");
    print_node(node);
    printf(");\n");
}

void print_blocks(struct ast_node* node)
{
    struct ast_node* n;
    int i = 0;

    while (NULL != (n = ast_get_nth_child(node, i++))) {
        print_node(n);
        printf(";\n");
    }
}

void print_id(struct ast_node* node)
{
    // id
    printf("%s", node->data.id.id);
    // tipo
    // st_get_symbol(node->data.id.id)->type;
    // printf("%s", node->data.id.type);
}

void print_declaration(struct ast_node* node){
    // id
    printf("%s", node->data.id.id);
    // tipo
    //st_get_symbol(node->data.id.id)->type;
    if(node->data.id.type == 1){
     printf("string");

    }else{
        if(node->data.id.type == 2){
            printf("number");
        }
    }
}

void print_if(struct ast_node* node)
{
    struct ast_node* n;

    printf("if(");
    // condicion
    n = ast_get_nth_child(node, 0);
    print_node(n);
    printf(") {\n");

    // then
    n = ast_get_nth_child(node, 1);
    print_node(n);
    // else?
    n = ast_get_nth_child(node, 2);
    if (n != NULL) {
        printf("} else {\n");
        print_node(n);
    }
    printf("}\n");
}

void print_while(struct ast_node* node){
    struct ast_node* n;
    printf("while(");
    // condicion
    n = ast_get_nth_child(node, 0);
    print_node(n);
    printf(") {\n");
    // then
    n = ast_get_nth_child(node, 1);
    print_node(n);
    printf("}\n");
}

void print_printer(struct ast_node* node)
{
    struct ast_node* n;

    printf("printf(\"");
    
    n = ast_get_nth_child(node, 0);
    print_node(n);
    n = ast_get_nth_child(node, 1);
    if(n != NULL){
        printf("\", ");
        print_node(n);
        printf(");\n");
    }else{
        printf("\");\n");
    }
}

void print_node(struct ast_node* node)
{
    if(node != NULL){
        switch (node->type) {
            case N_BLOCKS:
                print_blocks(node);
                break;
            case N_ID:
                print_id(node);
                break;
            case N_IF:
                print_if(node);
                break;
            case N_PRINTER:
                print_printer(node);
                break;
            case N_WHILE:
                print_while(node);
                break;
            case N_DECLARATION:
                print_declaration(node);
                break;
            // case N_SCAN:
            //     print_scan(node);
            //     break;
            // case N_LENGTH:
            //     print_length(node);
            //     break;
            // case N_CONST_STRING:
            //     print_const_string(node);
            //     break;
            case N_CONST_NUMBER:
                print_const_number(node);
                break;
            // case N_BOOLEAN:
            //     print_boolean(node);
            //     break;
            case N_OPERAND:
                print_operand(node);
                break;
            case N_OPERATION:
                print_operation(node);
                break;
            case N_EXPR:
                print_expr(node);
                break;
            case N_ASSIGN:
                print_assign(node);            
                break;
        }
    }
}

void print_ast(struct ast_node* ast)
{
    print_node(ast);
}

void print_st(int id, struct usr_st_data* data)
{  
    if (data->type == 2) {
        struct usr_st_data* array;
        array = ast_get_nth_child(data, 0);
        printf("double");    
        if(array != NULL){ //Imprimir tipo array si tiene un hijo asociado
            printf(" %s", data->name);        
            printf("[");
            printf("]");
            printf(";\n");
        }else{
            printf(" %s = 0;\n", data->name);        
        }
    }else{
        struct usr_st_data* array;
        array = ast_get_nth_child(data, 0);
        printf("string");    
        if(array != NULL){ //Imprimir tipo array si tiene un hijo asociado
            printf(" %s", data->name);        
            printf("[");
            printf("]");
            printf(";\n");
        }else{
            printf(" %s = '';\n", data->name);        
        }        
    }
    
}


int main(int argc, char **argv)
{
    if (argc > 1) {
        yyin = fopen(argv[1], "r");
    }

    if (yyparse()) {
        return 1;
    }
    printf("#include <stdio.h>\n");
    printf("#include <math.h>\n");
    printf("\n");
    printf("int main() {\n");

    st_print_symbols(print_st);
    printf("\n\n");

    print_ast(ast);

    printf("}\n");

    return 0;
}
