#ifndef FN_H
#define FN_H

#include "../lib/ast.h"

#define N_IF                    2
#define N_PRINTER				3
#define N_WHILE					4
#define N_DECLARATION			5
#define N_SCAN					6
#define N_LENGTH				7
#define N_ASSIGNMENT			8
#define N_ASSIGN 				9
// #define N_ARRAY 				8
// Prototipos
struct ast_node* n_if(struct ast_node* cond, struct ast_node* then, struct ast_node* _else);
struct ast_node* n_printer(struct ast_node* exp, struct ast_node* rec);
struct ast_node* n_while(struct ast_node* cond, struct ast_node* then);
struct ast_node* n_declaration(int _type, struct ast_node* _var, struct ast_node* _array);
//struct ast_node* n_declaration(struct ast_node* _type, struct ast_node* _var);
struct ast_node* n_assignment(int _type, struct ast_node* _var);
struct ast_node* n_declaration_array(struct ast_node* _type, struct ast_node* _var);
struct ast_node* n_scan(struct ast_node* _type, struct ast_node* _id);
struct ast_node* n_length(struct ast_node* _id);
struct ast_node* n_assign(char* id, struct ast_node* expr);
// struct ast_node* n_array(double _var);

#endif /* FN_H */