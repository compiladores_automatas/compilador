%{
	// Analizador Sintáctico - Parser	
	#include <stdio.h>
	#include "../lib/cya.h"
	#include "fn.h"
%}

%union{
	// Directiva %union: permite declarar los atri	butos que se van a asociar con los símbolos de
	// la gramática. Su efecto es equivalente a declarar una estructura de datos de tipo union en
	// el lenguaje C.
	char* id;
    double number;
    char* string;
    struct ast_node* node;
}

%token <id> T_ID
%token <number> T_NUM
%token <string> T_STRING T_CADENA
%token T_CORCH_A T_CORCH_C T_PAR_A T_PAR_C T_LLAVE_A T_LLAVE_C T_CHARAT T_COMA T_ECHO T_LEN T_READ T_SIZE T_START T_END
%token T_IGUAL T_MAS T_MENOS T_POT T_POR T_DIV T_CMP T_CAT T_AND T_OR T_MAY T_MIN T_MAY_IGUAL T_MIN_IGUAL T_DISTINTO T_ELSE T_IF T_WHILE T_CADENA T_IGUAL_IGUAL T_MEN

%type  <node> id array program block sentence declaration condition expression charpos size length printer print else printer_rec
%right T_IGUAL T_IGUAL_IGUAL
%left T_MAS T_MENOS
%left T_POR T_DIV
%left T_POT

%%

program:		T_START block T_END						{$$ = $2; ast = $2; }
				;
block: 			sentence block							{$$ = n_blocks($1, $2);}
				|										{$$ = n_blocks(NULL, NULL);}
				;
sentence:		declaration								{$$ = $1;}
				| print 								{$$ = $1;}
				| expression							{$$ = $1;}
				//| scan								{$$ = $1;}
				| length								{$$ = $1;}
				| size									{$$ = $1;}
				| charpos								{$$=NULL;}
				| T_IF T_PAR_A condition T_PAR_C T_LLAVE_A block T_LLAVE_C else	{$$ = n_if($3, $6, $8);}
				| T_WHILE T_PAR_A condition T_PAR_C T_LLAVE_A block T_LLAVE_C	{$$ = n_while($3, $6);}
				;
else:			T_ELSE T_LLAVE_A block T_LLAVE_C 		{$$ = $3;}
				|/*lambda*/								{$$=NULL;}
				;
declaration:	T_STRING id 							{$$ = n_declaration(1, $2, NULL); }
				| T_NUM id 								{$$ = n_declaration(2, $2, NULL); }
				| T_STRING id array						{$$ = n_declaration(1, $2, $3); }
				| T_NUM id array						{$$ = n_declaration(2, $2, $3); }
				; 				;
print:			T_ECHO T_PAR_A printer T_PAR_C			{$$ = $3;}
				;
printer:		expression printer_rec  				{$$ = n_printer($1, $2);}
				;
printer_rec:    T_COMA expression printer_rec			{$$ = n_printer($2, $3);}
				| /*lambda*/							{$$=NULL;}
				;
//scan:			T_READ T_PAR_A type T_COMA id T_PAR_C	{$$ = n_scan($3, $5);}
//				;
length:			T_LEN T_PAR_A id T_PAR_C				{$$ = n_length($3);}
				;
size:			T_SIZE T_PAR_A array T_PAR_C			{$$ = NULL;}
				;
charpos:		T_CHARAT T_PAR_A array T_PAR_C			{$$=NULL;}
				;
id:				T_ID 									{$$ = n_id($1, 0);}
				;
array:			T_CORCH_A T_NUM T_CORCH_C				{$$ = n_const_number($2);}
				;

charpos:		T_CHARAT T_PAR_A array T_COMA T_ID T_PAR_C	{$$=NULL;}
				;		
condition:		expression								{$$ = $1; }
				;
expression:		id										{$$ = $1; }
				| 
				T_NUM									{$$ = n_const_number($1);}
				| 
				array									{$$=NULL;}
				|
				T_CADENA								{$$ = n_const_string($1);}
				| 
				T_MENOS T_NUM							{$$ = NULL;}
				| 
				expression T_MAS expression				{$$ = n_operation(n_operand('+'),$1,$3);}
				| 
				expression T_MENOS expression			{$$ = n_operation(n_operand('-'),$1,$3);}
				| 	
				expression T_POT expression				{$$ = n_operation(n_operand('^'),$1,$3);}
				| 
				expression T_POR expression				{$$ = n_operation(n_operand('*'),$1,$3);}
				| 
				expression T_DIV expression				{$$ = n_operation(n_operand('/'),$1,$3);}
				| 
				expression T_CMP expression				{$$=NULL;}
				| 
				expression T_CAT expression				{$$=NULL;}
				|
				expression T_IGUAL_IGUAL expression		{$$ = n_operation(n_operand('1'),$1,$3);}
				|
				expression T_DISTINTO expression		{$$ = n_operation(n_operand('2'),$1,$3);}
				|
				expression T_MIN_IGUAL expression		{$$ = n_operation(n_operand('3'),$1,$3);}
				|
				expression T_MIN expression				{$$ = n_operation(n_operand('4'),$1,$3);}
				|
				expression T_MAY_IGUAL expression		{$$ = n_operation(n_operand('5'),$1,$3);}
				|
				expression T_MAY expression				{$$ = n_operation(n_operand('6'),$1,$3);}
				|
				expression T_AND expression				{$$ = n_operation(n_operand('7'),$1,$3);}
				|
				expression T_OR expression				{$$ = n_operation(n_operand('8'),$1,$3);}
				|
				T_ID T_IGUAL expression             	{$$ = n_assign($1, $3); }
				;

%%

void yyerror(char *s)
{
    syntax_error(st_line_number(), s);
}


